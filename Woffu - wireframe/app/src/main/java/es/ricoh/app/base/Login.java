package es.ricoh.app.base;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.example.MyClass;

import java.util.List;


public class Login extends Activity {

    private boolean mMultipleRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (getNumActivities(getPackageName()) > 1) {
            Log.i("LOGIN", "Another MainActivity instance is already running.");
            mMultipleRunning = true;
            finish();
            return;
        }

    }

    public void onClickOK(View v){
        Intent next = new Intent(this,MainActivity.class);
        startActivity(next);

    }


    public void onClickCancel(View v){
        Intent next = new Intent(this,MainActivity.class);
        startActivity(next);

    }


    @Override
    protected void onResume() {
        super.onResume();

        if (mMultipleRunning) {
            return;
        }
    }

    @Override
    protected void onDestroy() {
        Log.d("LOGIN", "onDestroy");
        super.onDestroy();

        // if MainActivity another instance is already running, then exit without doing anything
        if (mMultipleRunning) {
            return;
        }
    }

    private int getNumActivities(String packageName) {
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(30);
        for (ActivityManager.RunningTaskInfo info : list) {
            if (packageName.equals(info.topActivity.getPackageName())) {
                return info.numActivities;
            }
        }
        return 0;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //onDestroy();
        }
        return false;
    }
}
